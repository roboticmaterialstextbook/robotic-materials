%\documentclass[11pt]{scrbook} % use larger type; default would be 10pt

% WITH BLEED
% US Trade => 6x9, with a 0.125 bleed
% Adjust images size and gutter so tabs bleed by .125
% See https://www.createspace.com/Products/Book/InteriorPDF.jsp
\documentclass[paper=6.14in:9.21in,pagesize=pdftex,11pt,twoside,openright]{scrbook}
%openright
% Paper width
% W = 6.125in (6+0.125 --- bleed)
% Paper height
% H = 9.25in (9+2*.125 --- bleed)
% Paper gutter
% BCOR = 0.375in (0.5+0.5-0.625 --- margin with bleed)
% Margin (0.5in imposed on lulu, recommended on createspace)
% m = 0.625in (0.5+0.125 --- bleed)
% Text height
% h = H - 2m = 8in
% Text width
% w = W - 2m - BCOR = 4.5in
\areaset[0.375in]{4.5in}{8in}
\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%\usepackage{mitpress}
\usepackage{framed}

\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}

\usepackage{graphicx} % support the \includegraphics command and options
%\usepackage{wrapfig}

% \usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...
\usepackage{makeidx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[font={small,it}]{caption}
\usepackage{url}
\usepackage{hyperref}

\usepackage{todonotes}
\usepackage{tikz}
\usetikzlibrary{calc,decorations.markings}
\usepackage{xifthen}
\usepackage{amsfonts}
\usepackage{tabu}
%\usepackage{subcaption}
\usepackage{cite}
%\usepackage[cmex10]{amsmath}

\usepackage{stfloats}

\usepackage{diagbox}


%%% HEADERS & FOOTERS
%\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
%\pagestyle{fancy} % options: empty , plain , fancy
%\renewcommand{\headrulewidth}{0pt} % customise the layout...
%\lhead{}\chead{}\rhead{}
%\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
%\usepackage{sectsty}
%\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!

%%% END Article customizations

%%% The ``real'' document content comes below...
\makeindex


\begin{document}
%\maketitle

\thispagestyle{empty}
\begin{flushleft}
Nikolaus Correll, editor\\
Robotic Materials, 1st edition\\
ISBN-xx: xxx-xxxxxxxxxxxxxx
\end{flushleft}

\vfill

\begin{figure}[!h]
\includegraphics[width=1in]{figs/by-nc-nd}
\end{figure}

This book is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License. You are free to share, i.e., copy, distribute and transmit the work under the following conditions: You must attribute the work to its main author, you may not use this work for commercial purposes, and you may not alter, transform, or create derivatives of this work, except for written permission by the author. For more information, please consult \url{http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US}.


\cleardoublepage
\thispagestyle{empty}
\topskip0pt
\vspace*{\fill}
\begin{center}
\end{center}
\vspace*{\fill}

\tableofcontents

\chapter*{Preface}
Robotic Materials are neither a new idea nor a new field, yet it is unclear what one needs to know should one want to make, describe and analyze them. This book sets out to bring together disparate bodies of knowledge ranging from material science, robotics, electrical, aerospace engineering and computer science. While it cannot be comprehensive, this book's aim is to provide nomenclature and a common methodology to what I believe the materials of the future will be. 

In a nutshell, individual elements of future smart materials are hybrid automata that consist of discrete computation and material physics. All computing elements together will be making up a networking graph that is layered above the physically connected material system. As models are only as useful as the tools that they enable, and their availability is usually inversely proportional to the complexity of the model, this book aims at providing both a comprehensive view of robotic materials as well as examples of models with lesser complexity. 

The organization of this book is top-down, starting from the discrete mathematics describing the communication graph in a robotic material and then working its way down to material physics, control of an individual element and the hybrid automata connecting physical material and computation. I've chose this approach to immediately highlight the novel capabilities of robotic materials to the material scientist, whereas providing a comfortable environment to the computer scientist, from where most of my students start out. 

I'm grateful to my students who have helped me to make robotic materials a practical reality, the students in my ``Materials that think'' and ``Robotic Materials'' classes for contributing to the ideas and content of this book. Particular thanks goes to my program managers who helped me both shaping the vision of robotic materials and have supported me early: Byoung ``Les'' Lee, Airforce Office of Scientific Research, Richard Voyles, National Science Foundation, and Samuel Stanton at the Army Office of Research. 

\chapter{Introduction}
Advances in polymer science, miniaturization of computing, and manufacturing techniques have enabled a new class of robotic devices that embed sensing, computation, actuation and communication at high densities. This embedding can be so tight that the distinction between device and material blurs. This new class of materials has the ability to change its physical properties such as stiffness, density, weight, shape or appearance in response to external stimuli, and is able to adapt and learn. Here, the boundary between the dynamics of the physical system and control algorithms blurs, which allows trade-offs between morphological and silicon-based computation. Such materials shift the burden of integration and manufacturing from the designer to the foundry, enabling a new class of apparently simple robots made from functionalized stock materials such as rods, bars and sheets that can sense, and change their appearance and shape. 

The vision of functionalizing materials by embedding networked computation, sensing and actuation has been first articulated by \cite{berlin1997distributed} and was motivated by the advent of micro-electromechanical systems (MEMS). This work, summarizing the outcome of a DARPA workshop, has led to a series of influential follow-ups that laid the foundation for the field of sensor networks \cite{warneke2001smart} and amorphous computing \cite{abelson2000amorphous} that set out to ``create the system-architectural, algorithmic, and technological foundations for exploiting programmable materials.''
This paper describes a series of novel artifacts that we dub ``computational metamaterials'' (CMM).  

The term metamaterials has been coined by Walser in 2000 \cite{walser2000metamaterials}, to describe ``macroscopic composites having a manmade, three-dimensional, periodic cellular architecture designed to produce an optimized combination, not available in nature, of two or more responses to specific excitation'' in order to overcome the limitations of classical composite materials.  CMMs extend this definition by adding computation to create arbitrary relationships between sensing and actuation. CMMs are therefore subsets of amorphous computers, implemented in a lattice structure sharing power and communication locally.
 
The ultimate CMM consisting exclusively of mobile sensing, actuation, and computational elements has been introduced by Goldstein as ``Programmable Matter'' \cite{goldstein2005programmable} (not to be confused with the term introduced by Toffoli and Margolus \cite{toffoli1991programmable} to describe physics-inspired computation) and led to the claytronics project, which aims at casting materials into modular robots, with all the energy, structural, and mechanical problems this entails. In contrast, we wish CMMs to take advantage of the properties of the underlying polymer material, which can be both sensed and actuated upon. 

This chapter illustrates this concept and challenges that come with it using a series of examples that demonstrate high-bandwidth sensing, actuation via feedback control, and distributed computation in an integrated system. All of these systems are examples of challenges at which a material-centric approach excels by avoiding bottlenecks that high-bandwidth data has to get through and naturally scaling with the computational requirements of increasing number of sensors and actuators. 

%
%\section{Tactile Sensing in Flexible Artificial Skins}
\section{Tactile Sensing in Metamaterial Sheets}
The human skin implements a variety of sensors measuring both static and dynamic information with highly varying bandwidth such as pressure, shear, temperature, and textures \cite{johansson1979tactile}. We describe two distinct efforts for producing active polymer sheets with embedded sensing and computation at opposite ends of the perceptive spectrum. On the one hand, texture discrimination requires high temporal resolution with low spatial distribution of sensing sites. On the other hand, shape discrimination requires low temporal resolution with high spatial distribution of sensing sites. We describe the prototypes and fabrication processes of both types of skin.

\subsection{High-bandwidth sensing: texture recognition in skin}
Our current understanding of texture sensing requires sampling vibration signals at frequencies in the order of hundreds of Hertz, their spectral analysis, and subsequent classification \cite{dahiya2010tactile}. Replicating a system such as the human skin with its abilities to identify textures almost anywhere on the body therefore imposes serious engineering challenges that we argue can best be solved by a CMM that samples and processes high-bandwidth information locally and routes resulting high-level information to a central processing unit only when an important event occurs. 

We describe such a system in \cite{hughes14}, which consists of individual nodes equipped with a microphone, op-amp, and microcontroller that are networked with their 6-neighborhood, leading to a hexagonal lattice (Figure \ref{fig:skin}). Each microcontroller samples vibration signals recorded by the microphones at 1kHhz and performs a 256-bin Fast Fourier Transform (FFT) of the signal, which is then fed into a logistic regression that has been trained offline to differentiate 15 different textures. 

\begin{figure}[!htbp]
	\centering
		\includegraphics[width=\columnwidth]{figs/skin.png}
	\caption{System overview over a CMM that can identify and localize textures rubbed against it. PCBs and microphones are embedded in flexible rubber (EcoFlex) and suspended on a neoprene mesh.}
	\label{fig:skin}
\end{figure}

The signal processing chain is depicted in Figure \ref{fig:skinsp}. Note that the signal is modulated by the material itself before it gets sampled and further processed. Knowing the frequency dependent attenuation can therefore inform the necessary discretization of the CMM.

\begin{figure}
	\centering
		\includegraphics[width=\columnwidth]{figs/skinsp.png}
	\caption{Signal processing chain ranging from material-based attenuation to discretization, FFT, logistic regression and networking.}
	\label{fig:skinsp}
\end{figure}

We demonstrate texture recognition at 4Hz as well as triangulation of its localization by using intensity information recorded by at least three nodes at a time in \cite{hughes14}.  As all of these operations can happen in parallel, this approach’s scalability is only limited by the time it takes information from anywhere in the system to a sink, which scales with the square-root of the number of devices, assuming they are arranged on a disc with a data sink in its center. While the implementation of this particular CMM is bulky at present, future iterations of this system could be implemented on a single silicon wafer that can then be stretched and embedded in a composite as shown in \cite{salowitz2013bio}, see also Section \ref{sec:manufacturing}.

\subsection{High-density pressure sensing}
Intrinsic and extrinsic tactile sensing of force or pressure has been extensively explored for several decades \cite{Voyles96,Nicholls1989,Lee2010}. Our current understanding from human physiology suggests spatial sampling every 2mm to 20 mm can be useful for shape discrimination. We further prototyped a CMM for local sensing and detection of shape patterns based on pressure sensing of hexagonal tactels.

This tactile skin consists of a neuromorphic architecture comprised of multiple layers of neuronal circuitry for computation, a sensing medium for pressure/displacement, and an outer covering, as illustrated in Figure \ref{fig:skinSCPlayers}. The neuronal circuitry forms a perceptron-based artificial neural network that is interconnected across the substrate of the skin and can be programmed or can learn to perform specific computations. The work in \cite{nawrocki2011structured} describes synthetic neural networks that can be implemented using both polymer electronics  and conventional silicon electronics to demonstrate skin capabilities. Here, polymer neural networks that can be printed across the skin offer the capability to implement sensing arrays at high densities and include simple pre-processing, whereas stretchable electronics provides an opportunity for compressing information and routing it a to a central location.  

\begin{figure}
	\centering
		\includegraphics[width=\columnwidth]{figs/skinSCPlayers}
	\caption{Cutway visualization of a sheet of pressure-sensitive skin CMM with dielectric gel \cite{Voyles96} for sensing and/or actuation.}
	\label{fig:skinSCPlayers}
\end{figure}

%\begin{figure}
	%\centering
	%%	\includegraphics[width=\columnwidth]{figs/skinSCPprototype.jpg}
	%\caption{This prototype of pressure-sensitive skin uses a resistive medium for pressure sensing and conventional silicon electronic components to implement the neurons.}
	%\label{fig:skinSCPprototype}
%\end{figure}


\section{High-speed feedback control: shape changing rods}
Shape change of complex morphologies such as the human hand, a bird wing, an airfoil of the future, car chassis or adaptive robotic system requires the simultaneous control of many actuators. In biological systems part of this control is often involuntary, or reactive, and follows a larger pattern issued by the brain. CMMs have the potential to mimic these processes and perform local, high-speed control based on global patterns disseminated in the material. 

\begin{figure*}[!htb]
	\centering
		\includegraphics[width=\textwidth]{figs/shapechange.png}
	\caption{Concept drawing of a shape-changing CMM with embedded control (green discs). Low-melting point plastic bars with integrated heating coils and temperature sensor (middle, top) are arranged in a lattice and augmented with embedded controllers (middle, bottom). Shape can be changed by an external actuator, here pneumatic (right), and is exclusively determined by the stiffness profile and force.}
	\label{fig:shapechange}
\end{figure*}

We have been developing a class of CMMs \cite{mcevoy14} that have the ability to locally change their stiffness and shape (Figure \ref{fig:shapechange}) \cite{mcevoy14b}. These properties are related via the beam equation, which expresses the curvature of a beam as a function of its stiffness and bending force (Figure \ref{fig:shapechangesp}). We control stiffness by joule heating and melting a low-melting point polymer (Polycaprolactone), albeit other approaches and materials such as sheet jamming \cite{kim2012design} or Field’s metal \cite{shan2013soft} are possible. Each element of the CMM is therefore equipped with a heating coil, a temperature sensor, and a microcontroller to implement a simple feedback controller. Implementing a desired stiffness profile, which can later map into the corresponding shape profile, is therefore equivalent to disseminate a temperature profile into the CMM and locally ensuring that the temperature profile is met. Note that the temperature profile can be provided as a spatial function that can be locally resolved using a coordinate system that is trivial to establish in a grid networking topology. 

\begin{figure}
	\centering
		\includegraphics[width=1.00\columnwidth]{figs/shapechangesp.png}
	\caption{Global shape change due to local feedback control of stiffness illustrated using the 1D beam equation. Stiffness can be controlled by varying the temperature of a thermoplastic, e.g. }
	\label{fig:shapechangesp}
\end{figure}
 
Actuation is accomplished by applying an external moment using a single pneumatic actuator \cite{ilievski2011soft} or tendon actuator \cite{mcevoyiser14} extending along the entire length of the beam.

Shape change can also be accomplished by creating CMMs in which each individual element can bend \cite{correll2014soft}. A ring-shaped CMM consisting of eight elements, each equipped with dedicated sensing, computation and actuation, and networked with their neighbors to the left and right is shown in Figure \ref{fig:softbelt}. A forward rolling motion was achieved by evaluating a local photo sensor to determine whether a cell was facing the ground or not. A cell then inflated when it was facing the ground, but one of its neighbors did not. This led to a rolling motion of the mechanism. Other conformations of such an autonomous material are thinkable, leading to snakes, arms \cite{marchesedesign}, or sheets. Other approaches to shape change include folding via built-in shape memory alloys \cite{hawkes2010programmable}. 

\begin{figure*}[!htbp]
	\centering
		\includegraphics[width=0.9\textwidth]{figs/softbelt.PNG}
	\caption{Pneumatic belt rolling autonomously on a flat surface using distributed control.}
	\label{fig:softbelt}
\end{figure*}

Local feedback control, illustrated here using a shape-change example, is also relevant for appearance change, which can be achieved by embedding lights, but also passively by injecting liquids with desired optical or thermal properties \cite{farrow2013,morin2012camouflage}. 

\section{Manufacturing}\label{sec:manufacturing}
The systems shown in Figures \ref{fig:skin}, \ref{fig:shapechange} and \ref{fig:softbelt} all have been manufactured by a combination of printed circuit board (PCB) populated with off-the-shelf surface mounted devices (SMD), wired interconnects to provide power and communication between CMM elements, and embedding in a polymer material. Manufacturing these systems at scale requires overcoming a series of challenges that are due to the multitude of processes involved, the lack of automation, which is only available for subsets of the process, such as PCB edging and assembly, and finally compatibility of involved polymer materials.

Silicone materials such as EcoFlex do not bond with any other material. Embedding structural materials therefore requires either perforation, e.g., of a flexible circuit as in the system shown in Figure \ref{fig:softbelt}, or using a mesh structure as in Figure \ref{fig:skin} (Neoprene mesh) that provides sufficient the silicone to form sufficient cross linkages. After assembly of the PCB interconnects via wires (Figure \ref{fig:skin}) or soldering of flexible PCB (Figure \ref{fig:softbelt})and alignment of parts on a mesh carrier or mold, silicone based materials can be simply poured. Accommodating the pneumatic actuator channels in the system of Figure \ref{fig:softbelt}, required a design of a multi-piece mold, into which place holders for the pneumatic channels were inserted during curing \cite{correll2014soft}.

\begin{figure}
	\centering
		\includegraphics[width=0.8\columnwidth]{figs/IMG_0273.JPG}
	\caption{PCB attached to cloth. Interconnects are provided by conductive thread stitched using an automatic sewing machine.}
	\label{fig:thread}
\end{figure}

There are multiple possible solutions to automate the PCB interconnection process. In cloth-based wearable systems such as \cite{profita12}, interconnects can realized using conductive thread (Figure \ref{fig:thread}). While the sewing can be automated for the most part, affixing the thread to the PCB vias requires manual work. Ribbon cables, either from individual strands or flexible PCBs, are readily available in various lengths, but also require manual assembly and add significantly to the stand-off height of the PCB. In case all the required sensing, processing and actuation can be integrated into a single silicone die, interconnects can be designed in a spiral shape, which allows stretching the circuit array over multiple orders of magnitude. For example, \cite{salowitz2013bio,salowitz2013screen} describes an array of sensors and piezo-electric actuators that can be screen printed and embedded into a composite. \cite{xu2014soft} uses a similar method to embed sensing, computation and radio elements into a soft, conformable circuit that can be attached to the skin. Limitations of this approach include the limited number of interconnect wires that can run in parallel and the feasibility of processing complex circuits that require a variety of base materials.    

It is also possible to functionalize the carrier polymer, such as ABS plastic, such that a copper solution adheres to the surface. Entire circuits---including the capability of directly mounting SMD devices---created using this technique are known as \emph{Molded Interconnect Devices} (MID) \cite{islam2009process}. Electro-mechanical components, PCBs and interconnects can be aligned and integrated using \emph{Shape Deposition Manufacturing} (SDM) \cite{merz1994shape}. Here, place holders for electro-mechanical parts or interconnects are subtracted from a polymer or metal using a precision mill. These parts can then be embedded in the structure by depositing a polymer material, which can again be precision milled after curing. Finally, 3D printing allows creating structures with embedded conductive parts with the advent of carbon-infused ABS and PLA filaments, which are limiting due to their relatively high resistance of 10k$\Omega$/cm at 1.7mm diameter. 

All robotic materials described in this chapter implement control using conventional silicon-based electronics. The advent of polymer electronics, semi-conductors made exclusively from polymers, might enable a new generation of all-polymer computational metamaterials \cite{nawrocki2011structured}. A flexible, organic field effect transistor (OFET) that we manufactured is shown in Figure \ref{fig:ofet}. 

\begin{figure}
	\centering
		\includegraphics[width=0.8\columnwidth]{figs/ofet.jpg}
	\caption{Complete, four-neuron Synthetic Neural Network made of Organic Field Effect Transistors (OFETs) and Organic Memristive Read Only Memories (OM-ROMs) printed onto flexible substrate.}
	\label{fig:ofet}
\end{figure}

 Principally, polymer electronics can be manufactured in large sheets \cite{sondergaard2013roll}, but integrating circuits that are more complex than very few transistors or RFID tags, requires to overcome major challenges in material science, processing and manufacturing. 

\section{Chapter summary and organization of this book}
The systems presented in this chapter not only propose unique solutions to problems that suffer from scalability and bandwidth issues, they also demonstrate a very wide range of challenges that range from distributed control to manufacturing. 

This book tries to classify these problems, organizing this book into four parts: Mathematical foundations, algorithms, simulation, and manufacturing. The first part provides tools to formally describe a robotic material either using a discrete, continuous or hybrid view. This formal description lies the foundation for the analysis of algorithms and control in Part II. Due to the complexity of simulating large-scale distributed systems taking into account material dynamics, simulation of robotic materials deserves its own part. Finally, manufacturing related knowledge, as far as generalizable, is provided in Part IV. 

\chapter{Case Study: High-bandwidth sensing in texture-aware skin}

\input{skincasestudy/skinchapter.tex}

%\part{Mathematical foundations}
%\chapter{Discrete Aspects: Robotic Materials as spatial computers}
%From Chapter 2 of ``Spatial Computing'' by Matt Duckham.
%
%\chapter{Continuous Aspects: Material physics}
%Bending of beams. Sound propagation. Thermodynamics.
%
%\chapter{Robotic Materials as hybrid automata}
%Formal definition of hybrid automata from the ``Handbook of hybrid systems''. 
%
%\chapter{Robotic Materials as Amorphous Computers}
%Using Jake Beal's amorphous medium abstraction to describe robotic materials exclusively by vector fields. 
%
%\part{Algorithms and Control for Robotic Materials}
%\chapter{Algorithms for robotic materials}
%From Chapter 3 of ``Spatial Computing'' by Matt Duckham. Homa Hosseinmardi's TAAS paper. Sound source triangulation from Dana Hughes' texture localization paper. Something on learning in robotic materials.
%
%\part{Simulating Robotic Materials}
%\chapter{Simulation of discrete dynamics using NetLogo}
%\chapter{Simulation of continuous dynamics using Abaqus}
%\chapter{Simulation of hybrid automata}
%
%\part{Manufacturing}
%\chapter{Fiber-reinforced polymers}
%\chapter{Shape deposition manufacturing}
%\chapter{Soft robotics}
%\appendix
%\chapter{Discrete Math Primer}
%\chapter{Signals and Systems Primer}
%\chapter{Control theory Primer}

\bibliographystyle{alpha}
\bibliography{references}

\printindex

\end{document}


